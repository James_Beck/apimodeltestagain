﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JaggedArrayAPITest.Models;

namespace JaggedArrayAPITest.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string[]> Get()
        {
            TestModel first = new TestModel
            {
                FirstName = "James",
                LastName = "Beck",
                FavFood = "Pasta"
            };

            TestModel second = new TestModel
            {
                FirstName = "Mike",
                LastName = "Tindall",
                FavFood = "Chips"
            };

            TestModel third = new TestModel
            {
                FirstName = "Liam",
                LastName = "Mason-Webb",
                FavFood = "Roast Pork"
            };

            string[][] jag = new string[3][];

            jag[0] = new string[] { first.FirstName, first.LastName, first.FavFood };
            jag[1] = new string[] { second.FirstName, second.LastName, second.FavFood };
            jag[2] = new string[] { third.FirstName, third.LastName, third.FavFood };

            return jag;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
